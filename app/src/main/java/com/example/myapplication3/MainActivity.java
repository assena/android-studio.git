package com.example.myapplication3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceControl;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mTabweixin;
    private LinearLayout mTabfrd;
    private LinearLayout mTabcontact;
    private LinearLayout mTabsetting;

    private ImageButton mImgWeixin;
    private ImageButton mImgFrd;
    private ImageButton mImgcontact;
    private ImageButton mImgSettings;

    private android.app.Fragment mTab05;
    private android.app.Fragment mTab01 = new weixinFragment();
    private android.app.Fragment mTab02 = new frdFragment();
    private android.app.Fragment mTab03 = new contactFragment();
    private android.app.Fragment mTab04 = new settingsFragment();

    private TextView textView1,textView2,textView3,textView4;
    private android.app.FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        mTabweixin=findViewById(R.id.id_tab_weixin);
        mTabfrd=findViewById(R.id.id_tab_frd);
        mTabcontact=findViewById(R.id.id_tab_contact);
        mTabsetting=findViewById(R.id.id_tab_settings);

        initView();
        initEvent();
        initFragment();
        selectfragment(0);
    }
    private void initFragment(){
        fm = getFragmentManager();
        android.app.FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();
    }

    private void initEvent(){
        mTabweixin.setOnClickListener((View.OnClickListener) this);
        mTabfrd.setOnClickListener((View.OnClickListener) this);
        mTabcontact.setOnClickListener((View.OnClickListener) this);
        mTabsetting.setOnClickListener((View.OnClickListener) this);
    }

    private void initView() {
        mTabweixin = (LinearLayout)findViewById(R.id.id_tab_weixin);
        mTabfrd = (LinearLayout)findViewById(R.id.id_tab_frd);
        mTabcontact = (LinearLayout)findViewById(R.id.id_tab_contact);
        mTabsetting = (LinearLayout)findViewById(R.id.id_tab_settings);

        mImgWeixin = (ImageButton)findViewById(R.id.id_tab_weixin_img);
        mImgFrd = (ImageButton) findViewById(R.id.id_tab_frd_img);
        mImgcontact = (ImageButton)findViewById(R.id.id_tab_contact_img);
        mImgSettings = (ImageButton)findViewById(R.id.id_tab_settings_img);
    }

    private void selectfragment(int i){
        android.app.FragmentTransaction transaction = fm.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                Log.d("setSelect","1");
                transaction.show(mTab01);
                break;
            case 1:
                transaction.show(mTab02);
                break;
            case 2:
                transaction.show(mTab03);
                break;
            case 3:
                transaction.show(mTab04);
                break;
            default:
                break;
        }
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction) {
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.id_tab_weixin:
                selectfragment(0);
                break;
            case R.id.id_tab_frd:
                selectfragment(1);
                break;
            case R.id.id_tab_contact:
                selectfragment(2);
                break;
            case R.id.id_tab_settings:
                selectfragment(3);
                break;
        }
    }


}
